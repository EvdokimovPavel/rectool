#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include <fstream>
#include <streambuf>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <otlv4.h>

using namespace std;

void PrintVector(const vector<string>& v) {
	for (string s : v) {
        cout << s << "  ";
      }
	cout << endl;
}

void PrintMap(const map<string, vector<string> >& m) {
	cout << "Size = " << m.size() << endl;
	for (auto item: m) {
        cout << item.first << ": ";
        PrintVector(item.second);
      }
}

map<string,vector<string> > loadData(const string& type, const string& path) {
	if (type == "db") {
		map<string,vector<string> > res;
		vector<string> a;
		a.push_back("a");
		a.push_back("b");
		a.push_back("c");
		res["1"] = a;
		vector<string> b;
		b.push_back("d");
		b.push_back("e");
		b.push_back("f");
		res["2"] = b;
		return res;
	} else if (type == "csv") {
		map<string,vector<string> > res;
		fstream input(path);
		if (!input) {
			cout << "File not found! Path : " << path <<" \n";
		} else {
			string line;
			string id;
			while (getline(input,line)) {
				std::vector<std::string> strs;
				boost::split(strs,line,boost::is_any_of(","));
				id = strs[0];
				strs.erase(strs.begin());
				res[id] = strs;
		}
		return res;
	};
}
}

void reconcile(const map<string,vector<string> >& dset1, map<string,vector<string> >& dset2) {
	for (const auto& row : dset1) {
		if (dset2.count(row.first) == 0) {
			cout << "Row with id: " <<row.first <<" is missing in dset 2 but exists in dset1.\n";
		} else {
			for (int i = 0; i < row.second.size(); i++) {
				if (row.second[i] != dset2[row.first][i]) {
					cout << "Row with id: " <<row.first <<" has mismatch in column number: "<< i <<"\n";
				}
			}
			dset2.erase(row.first);
		}
	}

	for (const auto& item : dset2) {
		cout << "Row with id: " <<item.first <<" is missing in dset 1 but exists in dset2.\n";
	}
}

int main() {
	//cout << "PRIEM" << endl;
	string src1_type, src1;
	string src2_type, src2;

	cin >> src1_type;
	cin >> src1;
	cin >> src2_type;
	cin >> src2;

	cout << "TYPE1 : " << src1_type << endl;
	cout << "1 : " << src1 << endl;
	cout << "TYPE2 : " << src2_type << endl;
	cout << "2 : " << src2 << endl;

	map<string,vector<string> > dataset1 = loadData(src1_type,src1);
	map<string,vector<string> > dataset2 = loadData(src2_type,src2);

	PrintMap(dataset1);
	PrintMap(dataset2);

	reconcile(dataset1,dataset2);

	return 0;
}
